﻿using BuhoGestion.Views.General;
using BuhoGestion.Views.Login;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace BuhoGestion
{

    public partial class App : Application, ILoginManager
    {
        static ILoginManager loginManager;
        public new static App Current;
        public static int val;

        public App()

        {
            Current = this;
            bool isLoggedIn = Preferences.Get("IsLoggedIn", false);

            if (isLoggedIn)
            {

                MainPage = new MasterPageApp();
            }
            else
            {
                var nav = new NavigationPage(new LoginModalPage(this));
            nav.BarBackgroundColor = Color.Transparent;
            nav.BackgroundColor = Color.White;
            nav.BarTextColor = Color.White;
            nav.BackgroundColor = Color.FromHex("#f8f4fc");
            MainPage = nav;
            }

        }

        public void ShowMainPage()
        {
            MainPage = new MasterPageApp();
        }

        public void Logout()
        {
            Properties["IsLoggedIn"] = false;
            var nav = new NavigationPage(new LoginModalPage(this));
            nav.BarBackgroundColor = Color.Transparent;
            nav.BackgroundColor = Color.White;
            nav.BarTextColor = Color.White;
            nav.BackgroundColor = Color.FromHex("#f8f4fc");
            MainPage = nav;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
