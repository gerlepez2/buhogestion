﻿using BuhoGestion.Models;
using SQLite.Net.Async;
using SQLite.Net.Interop;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BuhoGestion.Data
{
    public class ServiceDatabase
    {
        public  SQLiteAsyncConnection _connection;

        public async Task InitializeAsync(string path, ISQLitePlatform sqlitePlatform)
        {
            _connection = SQLiteDatabase.GetConnection(path, sqlitePlatform);

            await _connection.CreateTableAsync<Deposito>();
       
        }

        //Deposito
        public async Task SaveAsyncDeposito(Deposito deposito)
        {
            var consultaDeposito = GetUnDeposito(deposito.idDeposito);

            var entity = new Deposito()
            {
                idDeposito = deposito.idDeposito,
                idRubro = deposito.idRubro,
                nombreDeposito = deposito.nombreDeposito,
                nroDeposito = deposito.nroDeposito,
                activo = deposito.activo,
            };

            if (consultaDeposito == null) 
            {      
                var count = await _connection.InsertAsync(entity);
               
            }
            else
            {
                var count = await _connection.UpdateAsync(entity);
                //return (count == 1) ? entity : null;
            }

        }

        public async Task<IEnumerable<Deposito>> GetAllAsyncDeposito()
        {
            var entities = await _connection.Table<Deposito>().ToListAsync();
            return entities;
        }

        public async Task<Deposito> GetUnDeposito(int idDeposito)
        {
            var deposito= await _connection.Table<Deposito>().Where(p => p.idDeposito == idDeposito).FirstOrDefaultAsync();
            return deposito;
        }

        public async Task DeleteDeposito(Deposito item)
        {
            await _connection.DeleteAsync(item);
        }
    }
}
