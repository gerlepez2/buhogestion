﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace BuhoGestion.Models
{
    public class Deposito
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idDepositoInterno { get; set; }
        public int idDeposito { get; set; }
        public string nombreDeposito { get; set; }
        public int nroDeposito { get; set; }
        public int idRubro { get; set; }
        public bool activo { get; set; }
    }
}
