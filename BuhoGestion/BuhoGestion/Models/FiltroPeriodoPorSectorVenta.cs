﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace BuhoGestion.Models
{
    public class FiltroPeriodoPorSectorVenta
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idFiltroPeriodoPorSectorVenta { get; set; }
        public int idMes { get; set; }
        public DateTime fechaDesde { get; set; }
        public DateTime fechaHasta { get; set; }
        public int cantidadDias { get; set; }
        public string mesAbreviado { get; set; }
        public int diasCorridos { get; set; }
        public int actual { get; set; }
        public int idSector { get; set; }
    }
}
