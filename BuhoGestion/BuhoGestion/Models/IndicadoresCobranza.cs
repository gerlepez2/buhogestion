﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace BuhoGestion.Models
{
    public class IndicadoresCobranza
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idIndicadoresCobranza { get; set; }
        public decimal montoCobrado { get; set; }
        public decimal montoObjetivo { get; set; }
        public decimal montoCuentaMorosa { get; set; }
        public decimal porcentajeEfectividad { get; set; }
        public int cantidadReclamo { get; set; }
        public string textoMontoObjetivo { get; set; }
        public string textoMontoCobrado { get; set; }
        public string textoPorcentajeEfectividad { get; set; }
        public string textoCantidadReclamo { get; set; }
        public string textoMontoCuentaMorosa { get; set; }
    }
}
