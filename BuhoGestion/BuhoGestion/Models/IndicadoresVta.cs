﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace BuhoGestion.Models
{
    public class IndicadoresVta
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idIndicadoresVta { get; set; }
        public decimal montoTicketPromedio { get; set;  }
        public decimal montoVendidoMesActual { get; set; }
        public decimal montoVendidoMesAnterior { get; set;  }
        public decimal porcentajeDiferencia { get; set; }
        public int cantidadNotaCredito { get; set; }
        public string productoMasVendido { get; set; }
        public string textoMontoVendidoActual { get; set; }
        public string textoMontoTicketPromedio { get; set; }
        public string textoProductoMasVendido { get; set; }
        public string textoMontoVendodMesAnterior { get; set; }
        public string textoPorcentajeDiferencia { get; set; }
        public string textoCantidadNotaCredito { get; set; }
    }
}
