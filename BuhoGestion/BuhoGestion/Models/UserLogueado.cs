﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace BuhoGestion.Models
{
    public class UserLogueado
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idUsuarioLogueado { get; set; }
        public int idUsuario { get; set; }
        public String nombreVendedor { get; set; }
        public String nombreEmpleado { get; set; }
        public int idPerfil { get; set; }
        //public int idEmpleado { get; set; }
    }
}
