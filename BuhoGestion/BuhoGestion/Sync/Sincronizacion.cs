﻿using BuhoGestion.WebServices;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace BuhoGestion.Sync
{
    public class Sincronizacion
    {

        HttpClient client = new HttpClient();

        public async Task<int> SincronizarClick()
        {
            var current = Connectivity.NetworkAccess;
            var profiles = Connectivity.ConnectionProfiles;

            if (current == NetworkAccess.Internet)
            {
                WSDeposito deposito = new WSDeposito();
                WSFiltrosIndicadorCobranza filtrosIndicadorCobranza = new WSFiltrosIndicadorCobranza();
                WSFiltrosIndicadorVenta filtrosIndicadorVenta = new WSFiltrosIndicadorVenta();
                WSindicadoresCobranza indicadoresCobranza = new WSindicadoresCobranza();
                WSindicadoresVtas indicadoresVenta = new WSindicadoresVtas();

                //filtros de venta
                var listaFiltroPeriodoPorSectorVenta = await filtrosIndicadorVenta.GetFiltroPeriodoPorSectorVenta<WSFiltrosIndicadorVenta>();
                var listaFiltroPorDepositoActivo = await filtrosIndicadorVenta.GetFiltroPorDepositoActivo<WSFiltrosIndicadorVenta>();

                //filtros cobranza
                var listaFiltroListarSectorCobranza = await filtrosIndicadorCobranza.GetFiltroListarSectorCobranza<WSFiltrosIndicadorCobranza>();
                var listaFiltroPeriodoPorSectorCobranza = await filtrosIndicadorCobranza.GetFiltroPeriodoPorSectorCobranza<WSFiltrosIndicadorCobranza>();

                //indicadores cobranza
                int idMes = 0; //pasamos el que selecciona del friltro
                int idEmpleado = 0; //vamos a enviar 0 hasta que la api del loguin lo devuelva, ahora devuelve null asi que el atributo esta comentado.
                var listaIndicadoresCobranza = await indicadoresCobranza.GetIndicadoresCobranza<WSindicadoresCobranza>(idMes, idEmpleado);

                //indicadores de venta
                int idmes = 1; //pasamos el que selecciona del friltro
                int idDeposito = 0; //pasamos el que selecciona del friltro
                var listaIndicadoresVenta = await indicadoresVenta.GetIndicadoresVtas<WSindicadoresVtas>(idmes, idDeposito);

                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
