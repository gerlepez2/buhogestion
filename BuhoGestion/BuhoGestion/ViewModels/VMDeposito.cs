﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuhoGestion.ViewModels
{
    public class VMDeposito
    {
        public int idDeposito { get; set; }
        public string nombreDeposito { get; set; }
        public int nroDeposito { get; set; }
        public int idRubro { get; set; }
        public bool activo { get; set; }
    }
}
