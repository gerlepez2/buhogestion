﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuhoGestion.ViewModels
{
    public class VMFiltroPorSectorCobranza
    {
        public int idVendedor { get; set; }
        public string  nombreVendedor { get; set; }
        public string apellidoVendedor { get; set; }
        public int idRol { get; set; }
        public bool activo { get; set; }
        public int idSector { get; set; }
        public DateTime fechaAlta { get; set; }
    }
}
