﻿using BuhoGestion.WebServices;
using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace BuhoGestion.ViewModels
{
    public class VMIndicadoresCobranza : BindableObject
    {
        public VMIndicadoresCobranza()
        {
            IdMes = 0;
            IdEmpleado = 0;
        }


        private int idIndicadoresCobranza { get; set; }

        private string montoCobrado { get; set; }
        private string montoObjetivo { get; set; }
        private string montoCuentaMorosa { get; set; }
        private decimal porcentajeEfectividad { get; set; }
        private int cantidadReclamo { get; set; }

        private string textoMontoObjetivo { get; set; }
        private string textoMontoCobrado { get; set; }
        private string textoPorcentajeEfectividad { get; set; }
        private string textoCantidadReclamo { get; set; }
        private string textoMontoCuentaMorosa { get; set; }

        private string imagenMontoCobrado { get; set; }
        private string imagenMontoObjetivo { get; set; }
        private string imagenMontoCuentaMorosa { get; set; }
        private string imagenPorcentajeEfectividad { get; set; }
        private string imagenCantidadReclamo { get; set; }

        private bool _isBusy;
        private string mensaje;
        private ICommand _reloadCommand;
        private int idMes;
        private int idEmpleado;
        //kate subo xq no esta lo q hice

        public int CantidadReclamo
        {
            get { return cantidadReclamo; }
            set
            {
                cantidadReclamo = value;
                OnPropertyChanged();
            }
        }

        public decimal PorcentajeEfectividad
        {
            get { return porcentajeEfectividad; }
            set
            {
                porcentajeEfectividad = value;
                OnPropertyChanged();
            }
        }

        public string MontoObjetivo
        {
            get { return montoObjetivo; }
            set
            {
                montoObjetivo = value;
                OnPropertyChanged();
            }
        }

        public string MontoCobrado
        {
            get { return montoCobrado; }
            set
            {
                montoCobrado = value;
                OnPropertyChanged();
            }
        }

        public string MontoCuentaMorosa
        {
            get { return montoCuentaMorosa; }
            set
            {
                montoCuentaMorosa = value;
                OnPropertyChanged();
            }
        }

        public string Mensaje
        {
            get { return mensaje; }
            set
            {
                mensaje = value;
                OnPropertyChanged();
            }
        }

        public int IdMes
        {
            get { return idMes; }
            set
            {
                idMes = value;
                OnPropertyChanged();
            }
        }

        public int IdEmpleado
        {
            get { return idEmpleado; }
            set
            {
                idEmpleado = value;
                OnPropertyChanged();
            }
        }

        public string TextoMontoObjetivo
        {
            get { return textoMontoObjetivo; }
            set
            {
                textoMontoObjetivo = value;
                OnPropertyChanged();
            }
        }

        public string TextoMontoCobrado
        {
            get { return textoMontoCobrado; }
            set
            {
                textoMontoCobrado = value;
                OnPropertyChanged();
            }
        }

        public string TextoPorcentajeEfectividad
        {
            get { return textoPorcentajeEfectividad; }
            set
            {
                textoPorcentajeEfectividad = value;
                OnPropertyChanged();
            }
        }

        public string TextoCantidadReclamo
        {
            get { return textoCantidadReclamo; }
            set
            {
                textoCantidadReclamo = value;
                OnPropertyChanged();
            }
        }

        public string TextoMontoCuentaMorosa
        {
            get { return textoMontoCuentaMorosa; }
            set
            {
                textoMontoCuentaMorosa = value;
                OnPropertyChanged();
            }
        }

        public string ImagenMontoCobrado
        {
            get { return imagenMontoCobrado; }
            set
            {
                imagenMontoCobrado = value;
                OnPropertyChanged();
            }
        }

        public string ImagenMontoObjetivo
        {
            get { return imagenMontoObjetivo; }
            set
            {
                imagenMontoObjetivo = value;
                OnPropertyChanged();
            }
        }

        public string ImagenMontoCuentaMorosa
        {
            get { return imagenMontoCuentaMorosa; }
            set
            {
                imagenMontoCuentaMorosa = value;
                OnPropertyChanged();
            }
        }

        public string ImagenPorcentajeEfectividad
        {
            get { return imagenPorcentajeEfectividad; }
            set
            {
                imagenPorcentajeEfectividad = value;
                OnPropertyChanged();
            }
        }

        public string ImagenCantidadReclamo
        {
            get { return imagenCantidadReclamo; }
            set
            {
                imagenCantidadReclamo = value;
                OnPropertyChanged();
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        public ICommand ReloadCommand =>
    _reloadCommand ??
    (_reloadCommand = new Command(async () => await GetInidcadoresCobranzaAsync()));

        public async Task GetInidcadoresCobranzaAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            try
            {
                var current = Connectivity.NetworkAccess;
                var profiles = Connectivity.ConnectionProfiles;
                WSindicadoresCobranza indicadoresCobranza = new WSindicadoresCobranza();


                if (current == NetworkAccess.Internet)
                {
                    //int idmes = 1; //pasamos el que selecciona del friltro
                    //int idEmpleado = 0; //pasamos el que selecciona del friltro
                    var indicCob = await indicadoresCobranza.GetIndicadoresCobranza<WSindicadoresCobranza>(IdMes, IdEmpleado);

                    MontoCobrado = String.Format("{0:n0}", indicCob.montoCobrado).Replace(NumberFormatInfo.CurrentInfo.NumberGroupSeparator, ".");
                    MontoObjetivo = String.Format("{0:n0}", indicCob.montoObjetivo).Replace(NumberFormatInfo.CurrentInfo.NumberGroupSeparator, ".");
                    MontoCuentaMorosa = String.Format("{0:n0}", indicCob.montoCuentaMorosa).Replace(NumberFormatInfo.CurrentInfo.NumberGroupSeparator, ".");
                    PorcentajeEfectividad = indicCob.porcentajeEfectividad;
                    CantidadReclamo = indicCob.cantidadReclamo;
                    TextoCantidadReclamo = indicCob.textoCantidadReclamo;
                    TextoMontoCobrado = indicCob.textoMontoCobrado;
                    TextoMontoCuentaMorosa = indicCob.textoMontoCuentaMorosa;
                    TextoMontoObjetivo = indicCob.textoMontoObjetivo;
                    TextoPorcentajeEfectividad = indicCob.textoPorcentajeEfectividad;
                    ImagenCantidadReclamo = "reclamo.png";
                    ImagenMontoCobrado = "cobrado.png";
                    ImagenMontoCuentaMorosa = "morosos.png";
                    ImagenMontoObjetivo = "objetivo.png";
                    ImagenPorcentajeEfectividad = "efectividad.png";
                }
            }
            catch (Exception ex)
            {
                mensaje = "Actualmente no se puede obtener Indicadores de Cobranza";
            }
            finally
            {
                IsBusy = false;
            }


        }
    }
}
