﻿using BuhoGestion.WebServices;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace BuhoGestion.ViewModels
{
    public class VMIndicadoresVta : BindableObject
    {
        public VMIndicadoresVta()
        {
            Idmes = 0;
            IdDeposito = 0;
        }

        public int idIndicadoresVta { get; set; }

        private string imagenMontoTicketPromedio  { get; set; }
        private string imagenMontoVendidoMesActual { get; set; }
        private string imagenMontoVendidoMesAnterior { get; set; }
        private string imagenPorcentajeDiferencia { get; set; }
        private string imagenCantidadNotaCredito { get; set; }
        private string imagenProductoMasVendido { get; set; }

        private string montoTicketPromedio { get; set; }
        private string montoVendidoMesActual { get; set; }
        private string montoVendidoMesAnterior { get; set; }
        private decimal porcentajeDiferencia { get; set; }
        private int cantidadNotaCredito { get; set; }
        private string productoMasVendido { get; set; }

        private string textoMontoVendidoActual { get; set; }
        private string textoMontoTicketPromedio { get; set; }
        private string textoProductoMasVendido { get; set; }
        private string textoMontoVendodMesAnterior { get; set; }
        private string textoPorcentajeDiferencia { get; set; }
        private string textoCantidadNotaCredito { get; set; }

        private bool _isBusy;
        private ICommand _reloadCommand;
        private string mensaje;
        private int idmes;
        private int idDeposito;

        public string ImagenMontoTicketPromedio
        {
            get { return imagenMontoTicketPromedio; }
            set
            {
                imagenMontoTicketPromedio = value;
                OnPropertyChanged();
            }
        }

        public string ImagenMontoVendidoMesActual
        {
            get { return imagenMontoVendidoMesActual; }
            set
            {
                imagenMontoVendidoMesActual = value;
                OnPropertyChanged();
            }
        }

        public string ImagenMontoVendidoMesAnterior
        {
            get { return imagenMontoVendidoMesAnterior; }
            set
            {
                imagenMontoVendidoMesAnterior = value;
                OnPropertyChanged();
            }
        }

        public string ImagenPorcentajeDiferencia
        {
            get { return imagenPorcentajeDiferencia; }
            set
            {
                imagenPorcentajeDiferencia = value;
                OnPropertyChanged();
            }
        }

        public string ImagenCantidadNotaCredito
        {
            get { return imagenCantidadNotaCredito; }
            set
            {
                imagenCantidadNotaCredito = value;
                OnPropertyChanged();
            }
        }

        public string ImagenProductoMasVendido
        {
            get { return imagenProductoMasVendido; }
            set
            {
                imagenProductoMasVendido = value;
                OnPropertyChanged();
            }
        }

        public string TextoMontoVendidoActual
        {
            get { return textoMontoVendidoActual; }
            set
            {
                textoMontoVendidoActual = value;
                OnPropertyChanged();
            }
        }

        public string TextoMontoTicketPromedio
        {
            get { return textoMontoTicketPromedio; }
            set
            {
                textoMontoTicketPromedio = value;
                OnPropertyChanged();
            }
        }

        public string TextoProductoMasVendido
        {
            get { return textoProductoMasVendido; }
            set
            {
                textoProductoMasVendido = value;
                OnPropertyChanged();
            }
        }

        public string TextoMontoVendodMesAnterior
        {
            get { return textoMontoVendodMesAnterior; }
            set
            {
                textoMontoVendodMesAnterior = value;
                OnPropertyChanged();
            }
        }

        public string TextoPorcentajeDiferencia
        {
            get { return textoPorcentajeDiferencia; }
            set
            {
                textoPorcentajeDiferencia = value;
                OnPropertyChanged();
            }
        }

        public string TextoCantidadNotaCredito
        {
            get { return textoCantidadNotaCredito; }
            set
            {
                textoCantidadNotaCredito = value;
                OnPropertyChanged();
            }
        }

        public string ProductoMasVendido
        {
            get { return productoMasVendido; }
            set
            {
                productoMasVendido = value;
                OnPropertyChanged();
            }
        }

        public int CantidadNotaCredito
        {
            get { return cantidadNotaCredito; }
            set
            {
                cantidadNotaCredito = value;
                OnPropertyChanged();
            }
        }

        public decimal PorcentajeDiferencia
        {
            get { return porcentajeDiferencia; }
            set
            {
                porcentajeDiferencia = value;
                OnPropertyChanged();
            }
        }

        public string MontoVendidoMesAnterior
        {
            get { return montoVendidoMesAnterior; }
            set
            {
                montoVendidoMesAnterior = value;
                OnPropertyChanged();
            }
        }

        public string MontoVendidoMesActual
        {
            get { return montoVendidoMesActual; }
            set
            {
                montoVendidoMesActual = value;
                OnPropertyChanged();
            }
        }

        public string MontoTicketPromedio
        {
            get { return montoTicketPromedio; }
            set
            {
                montoTicketPromedio = value;
                OnPropertyChanged();
            }
        }

        public string Mensaje
        {
            get { return mensaje; }
            set
            {
                mensaje  = value;
                OnPropertyChanged();
            }
        }

        public int Idmes
        {
            get { return idmes; }
            set
            {
                idmes = value;
                OnPropertyChanged();
            }
        }

        public int IdDeposito
        {
            get { return idDeposito; }
            set
            {
                idDeposito = value;
                OnPropertyChanged();
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        public ICommand ReloadCommand =>
    _reloadCommand ??
    (_reloadCommand = new Command(async () => await GetInidcadoresVentaAsync()));

        public async Task GetInidcadoresVentaAsync()
        {
            if (IsBusy)
                return;


            IsBusy = true;
            try
            {
                var current = Connectivity.NetworkAccess;
                var profiles = Connectivity.ConnectionProfiles;
                WSindicadoresVtas indicadoresVenta = new WSindicadoresVtas();

                if (current == NetworkAccess.Internet)
                {
                    //int idmes = 1; //pasamos el que selecciona del friltro
                    //int idDeposito = 0; //pasamos el que selecciona del friltro
                    var indicadoresVta = await indicadoresVenta.GetIndicadoresVtas<WSindicadoresVtas>(Idmes, IdDeposito);

                    TextoMontoVendidoActual = indicadoresVta.textoMontoVendidoActual;
                    TextoCantidadNotaCredito = indicadoresVta.textoCantidadNotaCredito;
                    TextoMontoTicketPromedio = indicadoresVta.textoMontoTicketPromedio;
                    TextoMontoVendodMesAnterior = indicadoresVta.textoMontoVendodMesAnterior;
                    TextoPorcentajeDiferencia = indicadoresVta.textoPorcentajeDiferencia;
                    TextoProductoMasVendido = indicadoresVta.textoProductoMasVendido;
                    MontoTicketPromedio = String.Format("{0:n0}", indicadoresVta.montoTicketPromedio).Replace(NumberFormatInfo.CurrentInfo.NumberGroupSeparator, "."); 
                    MontoVendidoMesActual = String.Format("{0:n0}", indicadoresVta.montoVendidoMesActual).Replace(NumberFormatInfo.CurrentInfo.NumberGroupSeparator, ".");
                    MontoVendidoMesAnterior = String.Format("{0:n0}", indicadoresVta.montoVendidoMesAnterior).Replace(NumberFormatInfo.CurrentInfo.NumberGroupSeparator, ".");
                    PorcentajeDiferencia = indicadoresVta.porcentajeDiferencia;
                    CantidadNotaCredito = indicadoresVta.cantidadNotaCredito;
                    ProductoMasVendido = indicadoresVta.productoMasVendido;
                    ImagenCantidadNotaCredito = "NotaCredito.png";
                    ImagenMontoTicketPromedio = "TicketPromedio.png";
                    ImagenMontoVendidoMesActual = "mesActual.png";
                    ImagenMontoVendidoMesAnterior = "mesAnterior.png";
                    ImagenPorcentajeDiferencia = "diferencia.png";
                    ImagenProductoMasVendido = "MasVendido.png";
                }
            }
            catch (Exception ex)
            {
                var a = ex;
                mensaje = "Actualmente no se puede obtener Indicadores de Venta";
            }
            finally
            {
                IsBusy = false;
            }


        }
    }
}
