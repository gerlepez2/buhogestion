﻿using BuhoGestion.WebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BuhoGestion.Models;

namespace BuhoGestion.Views.Cobranza
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FiltroIndicadoresCobranza : ContentPage
	{
        List<FiltroPorSectorCobranza> listaFiltroListarSectorCobranza = new List<FiltroPorSectorCobranza>();
        List<FiltroPeriodoPorSectorCobranza> listaFiltroPeriodoPorSectorCobranza = new List<FiltroPeriodoPorSectorCobranza>();
        public FiltroIndicadoresCobranza ()
		{
			InitializeComponent ();

        }

        protected override async void OnAppearing()
        {
            this.Content.IsEnabled = false;
            indi.IsRunning = true;
            var current = Connectivity.NetworkAccess;
            var profiles = Connectivity.ConnectionProfiles;

            if (current == NetworkAccess.Internet)
            {
                pickerFecha.Items.Add("Todos");
                pickerEmpleado.Items.Add("Todos");


                WSFiltrosIndicadorCobranza filtrosIndicadorCobranza = new WSFiltrosIndicadorCobranza();
                listaFiltroListarSectorCobranza = await filtrosIndicadorCobranza.GetFiltroListarSectorCobranza<WSFiltrosIndicadorCobranza>();
                listaFiltroPeriodoPorSectorCobranza = await filtrosIndicadorCobranza.GetFiltroPeriodoPorSectorCobranza<WSFiltrosIndicadorCobranza>();

                foreach (var item in listaFiltroPeriodoPorSectorCobranza)
                {
                    pickerFecha.Items.Add(item.mesAbreviado);

                }

                foreach (var item in listaFiltroListarSectorCobranza)
                {
                    pickerEmpleado.Items.Add(item.apellidoVendedor+" "+item.nombreVendedor);

                }

                pickerFecha.SelectedItem = "Todos";
                pickerEmpleado.SelectedItem = "Todos";
                this.Content.IsEnabled = true;
                indi.IsRunning = false;


            }
            else
            {
                this.Content.IsEnabled = true;
                indi.IsRunning = false;
                await DisplayAlert("", "No posee conexión a internet, intente mas tarde", "ok");
                await Navigation.PopModalAsync();
            }
        }

        public async void Filtrar(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            indi.IsRunning = true;
            if (pickerEmpleado.SelectedItem != null)
            {
                if (pickerFecha.SelectedItem != null)
                {
                    int idMes = 0;
                    int idEmpleado =0;
                    if (pickerFecha.SelectedIndex == 0)
                    {
                        idMes = 0;
                    }
                    else
                    {
                        for (int i = 0; i < listaFiltroPeriodoPorSectorCobranza.Count; i++)
                        {
                            if (pickerFecha.SelectedIndex == i + 1)
                            {
                                idMes = listaFiltroPeriodoPorSectorCobranza[i].idMes;
                            }
                        }
                    }
                    if (pickerEmpleado.SelectedIndex == 0)
                    {
                        idEmpleado = 0;
                    }
                    else
                    {
                        for (int i = 0; i < listaFiltroListarSectorCobranza.Count; i++)
                        {
                            if (pickerEmpleado.SelectedIndex == i + 1)
                            {
                                idEmpleado = listaFiltroListarSectorCobranza[i].idVendedor;
                            }
                        }
                    }
                    List<int> lista = new List<int>();
                    lista.Add(idMes);
                    lista.Add(idEmpleado);
                    MessagingCenter.Send(this, "filtrosCob", lista);
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    await Navigation.PopModalAsync();
                }
                else
                {
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    await DisplayAlert("", "Debe seleccionar alguna fecha", "ok");

                }
            }
            else
            {
                this.Content.IsEnabled = true;
                indi.IsRunning = false;
                await DisplayAlert("", "Debe seleccionar algún empleado", "ok");
            }

        }
    }
}