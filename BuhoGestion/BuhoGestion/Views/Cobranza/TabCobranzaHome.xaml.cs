﻿using BuhoGestion.Sync;
using BuhoGestion.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BuhoGestion.Views.Cobranza
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TabCobranzaHome : ContentPage
	{
        int idMes = 0;
        int idEmpleado = 0;

		public TabCobranzaHome ()
		{
			InitializeComponent ();
            mensaje();
            BindingContext = new VMIndicadoresCobranza();

        }

        protected override async void OnAppearing()
        {
            if (BindingContext is VMIndicadoresCobranza)
            {
                await ((VMIndicadoresCobranza)BindingContext).GetInidcadoresCobranzaAsync();
            }

            string msj = ((VMIndicadoresCobranza)BindingContext).Mensaje;
            if (msj != null)
            {
               await DisplayAlert("", msj, "ok");
            }
            base.OnAppearing();
        }

        public void Filter(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new FiltroIndicadoresCobranza());

        }

        public void mensaje()
        {
            MessagingCenter.Subscribe<FiltroIndicadoresCobranza, List<int>>(this, "filtrosCob", async (page, lista) =>
            {
                idMes = lista[0];
                idEmpleado = lista[1];

                ((VMIndicadoresCobranza)BindingContext).IdMes = idMes;
                ((VMIndicadoresCobranza)BindingContext).IdEmpleado = idEmpleado;


                //if (BindingContext is VMIndicadoresCobranza)
                //{
                //    await ((VMIndicadoresCobranza)BindingContext).GetInidcadoresCobranzaAsync();
                //}

                //string msj = ((VMIndicadoresCobranza)BindingContext).Mensaje;
                //if (msj != null)
                //{
                //    await DisplayAlert("", msj, "ok");
                //}
            });
        }

    }
}