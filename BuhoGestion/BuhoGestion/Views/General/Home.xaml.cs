﻿using BuhoGestion.Sync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BuhoGestion.Views.General
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Home : TabbedPage
    {
        public Home()
        {
            InitializeComponent();
         
        }

        protected override bool OnBackButtonPressed()
        {
            if (this.Navigation.NavigationStack.Count == 1)
            {
                CerrarSesion();

            }
            else
            {
                this.Navigation.PopAsync();
            }
            return true;
        }

        private async void CerrarSesion()
        {
            var answer = await DisplayAlert("", "¿Desea cerrar sesión?", "Si", "No");
            if (answer)
            {
                App.Current.Logout();
            }
        }

    }

}