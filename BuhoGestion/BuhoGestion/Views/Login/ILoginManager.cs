﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuhoGestion.Views.Login
{
    public interface ILoginManager
    {
        void ShowMainPage();
        void Logout();
    }
}
