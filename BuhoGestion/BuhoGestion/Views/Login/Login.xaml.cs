﻿using System;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using BuhoGestion.WebServices;
using BuhoGestion.Sync;

namespace BuhoGestion.Views.Login
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public static string usuario = "";
        public static string password = "";
        ILoginManager iml = null;

        string Url = "http://biancogestion.com/api/Usuario/Verificar?";

        static HttpClient _client = new HttpClient();

        protected override void OnAppearing()
        {
            base.OnAppearing();
            NavigationPage.SetHasNavigationBar(this, false);
            var currentVersion = VersionTracking.CurrentVersion;
            version.Text = currentVersion;
        }

        public Login(ILoginManager ilm)
        {
            InitializeComponent();
            iml = ilm;
        }

        async void btnLoginClick(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            indi.IsRunning = true;
            usuario = Username.Text;
            password = Password.Text;
            if (!string.IsNullOrEmpty(usuario))
            {
                if (!string.IsNullOrEmpty(password))
                {
                    WSLogin login = new WSLogin();
                    //if (current == NetworkAccess.Internet)
                    //{
                    var result = await login.GetUsuarioLogueado<WSLogin>(Url + "usuario=" + usuario + "&password=" + password);
                    
                    if (result != null)
                    {
                        Preferences.Set("Name", result.ToString());
                        Preferences.Set("vendedor", result.ToString());
                        Preferences.Set("IsLoggedIn", true);
                        var idusuarioLogueado = Preferences.Get("idusuarioLogueado", 0);

                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                        iml.ShowMainPage();
                    }

                    else
                    {
                        await DisplayAlert("", "Usuario y/o contraseña incorrecto. Por favor intente otra vez", "Ok");

                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                    }
                }
                else
                {
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    await DisplayAlert("", "Por favor ingrese su contraseña", "ok");

                }

            }
            else
            {
                this.Content.IsEnabled = true;
                indi.IsRunning = false;
                await DisplayAlert("", "Por favor ingrese su usuario", "ok");
            }

        }
    }
}