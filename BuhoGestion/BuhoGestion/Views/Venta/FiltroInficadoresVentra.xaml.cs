﻿using BuhoGestion.WebServices;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BuhoGestion.Models;
using System.Collections.Generic;

namespace BuhoGestion.Views.Venta
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FiltroInficadoresVentra : ContentPage
	{
        List<FiltroPeriodoPorSectorVenta> listaFiltroPeriodoPorSectorVenta = new List<FiltroPeriodoPorSectorVenta>();
        List<Deposito> listaFiltroPorDepositoActivo = new List<Deposito>();


        public FiltroInficadoresVentra ()
		{
			InitializeComponent ();
        }

        protected override async void OnAppearing()
        {
            this.Content.IsEnabled = false;
            indi.IsRunning = true;
            var current = Connectivity.NetworkAccess;
            var profiles = Connectivity.ConnectionProfiles;

            if (current == NetworkAccess.Internet)
            {
                pickerFecha.Items.Add("Todos");
                pickerDeposito.Items.Add("Todos");


                WSFiltrosIndicadorVenta filtrosIndicadorVenta = new WSFiltrosIndicadorVenta();
                listaFiltroPeriodoPorSectorVenta = await filtrosIndicadorVenta.GetFiltroPeriodoPorSectorVenta<WSFiltrosIndicadorVenta>();
                listaFiltroPorDepositoActivo = await filtrosIndicadorVenta.GetFiltroPorDepositoActivo<WSFiltrosIndicadorVenta>();

                foreach (var item in listaFiltroPeriodoPorSectorVenta)
                {
                    pickerFecha.Items.Add(item.mesAbreviado);

                }

                foreach (var item in listaFiltroPorDepositoActivo)
                {
                    pickerDeposito.Items.Add(item.nombreDeposito);

                }

                pickerFecha.SelectedItem = "Todos";
                pickerDeposito.SelectedItem = "Todos";

                this.Content.IsEnabled = true;
                indi.IsRunning = false;
            }
            else
            {
                this.Content.IsEnabled = true;
                indi.IsRunning = false;
                await DisplayAlert("","No posee conexión a internet, intente mas tarde","ok");
                await Navigation.PopModalAsync();
            }
        }


        public async void Filtrar(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            indi.IsRunning = true;

            if (pickerDeposito.SelectedItem != null)
            {
                if(pickerFecha.SelectedItem != null)
                {
                    int idMes = 0;
                    int idDeposito = 0;
                    if (pickerFecha.SelectedIndex == 0)
                    {
                        idMes = 0;
                    }
                    else
                    {
                        for (int i = 0; i < listaFiltroPeriodoPorSectorVenta.Count; i++)
                        {
                            if(pickerFecha.SelectedIndex == i + 1)
                            {
                                idMes = listaFiltroPeriodoPorSectorVenta[i].idMes;
                            }
                        }
                    }
                    if (pickerDeposito.SelectedIndex == 0)
                    {
                        idDeposito = 0;
                    }
                    else
                    {
                        for (int i = 0; i < listaFiltroPorDepositoActivo.Count; i++)
                        {
                            if (pickerDeposito.SelectedIndex == i + 1)
                            {
                                idDeposito = listaFiltroPorDepositoActivo[i].idDeposito;
                            }
                        }
                    }
                    List<int> lista = new List<int>();
                    lista.Add(idMes);
                    lista.Add(idDeposito);
                    MessagingCenter.Send(this, "filtrosVta", lista);
                    await Navigation.PopModalAsync();
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                }
                else
                {
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    await DisplayAlert("", "Debe seleccionar alguna fecha", "ok");

                }
            }
            else
            {
                this.Content.IsEnabled = true;
                indi.IsRunning = false;
                await DisplayAlert("","Debe seleccionar algún depósito","ok");
            }

        }
    }
}