﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BuhoGestion.ViewModels;
using System.Collections.Generic;

namespace BuhoGestion.Views.Venta
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TabVentaHome : ContentPage
	{
        int idMes = 0;
        int idDeposito = 0;

		public TabVentaHome ()
		{
			InitializeComponent ();
            mensaje();
            BindingContext = new VMIndicadoresVta();
        }

        protected override async void OnAppearing()
        {
            if (BindingContext is VMIndicadoresVta)
            {
                await ((VMIndicadoresVta)BindingContext).GetInidcadoresVentaAsync();
            }

            string msj = ((VMIndicadoresVta)BindingContext).Mensaje;
            if (msj != null)
            {
                await DisplayAlert("", msj, "ok");
            }
            
            base.OnAppearing();
        }

        public void Filter(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new FiltroInficadoresVentra());

        }

        public void mensaje()
        {
            MessagingCenter.Subscribe<FiltroInficadoresVentra, List<int>>(this, "filtrosVta",async (page, lista) =>
            {
                idMes = lista[0];
                idDeposito = lista[1];
                ((VMIndicadoresVta)BindingContext).Idmes = idMes;
                ((VMIndicadoresVta)BindingContext).IdDeposito = idDeposito;


                //if (BindingContext is VMIndicadoresVta)
                //{
                //    await ((VMIndicadoresVta)BindingContext).GetInidcadoresVentaAsync();
                //}

                //string msj = ((VMIndicadoresVta)BindingContext).Mensaje;
                //if (msj != null)
                //{
                //    await DisplayAlert("", msj, "ok");
                //}
            });
        }


    }
}