﻿using BuhoGestion.Models;
using BuhoGestion.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BuhoGestion.WebServices
{
    public class WSFiltrosIndicadorCobranza
    {
        HttpClient client = new HttpClient();

        public async Task<List<FiltroPeriodoPorSectorCobranza>> GetFiltroPeriodoPorSectorCobranza<T>()
        {
            String url = "http://biancogestion.com/api/General/GetPeriodoPorSector?idSector=2";
            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var listado = JsonConvert.DeserializeObject<List<FiltroPeriodoPorSectorCobranza>>(content);
                    return listado;
                }
                catch (Exception e)
                {
                    var a = e;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
       

        public async Task<List<FiltroPorSectorCobranza>> GetFiltroListarSectorCobranza<T>()
        {
            String url = "http://biancogestion.com/api/Vendedor/GetListarSector?idSector=2";
            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var listado = JsonConvert.DeserializeObject<List<FiltroPorSectorCobranza>>(content);
                    return listado;
                }
                catch (Exception e)
                {
                    var a = e;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
