﻿using BuhoGestion.Models;
using BuhoGestion.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BuhoGestion.WebServices
{
    public class WSFiltrosIndicadorVenta
    {
       HttpClient client = new HttpClient();

        public async Task<List<FiltroPeriodoPorSectorVenta>> GetFiltroPeriodoPorSectorVenta<T>()
        {
            String url = "http://biancogestion.com/api/General/GetPeriodoPorSector?idSector=1";
            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var listado = JsonConvert.DeserializeObject<List<FiltroPeriodoPorSectorVenta>>(content);
                    return listado;
                }
                catch (Exception e)
                {
                    var a = e;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public async Task<List<Deposito>> GetFiltroPorDepositoActivo<T>()
        {
            String url = "http://biancogestion.com/api/Deposito/GetListarActivo";
            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var listado = JsonConvert.DeserializeObject<List<Deposito>>(content);
                    return listado;
                }
                catch (Exception e)
                {
                    var a = e;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
