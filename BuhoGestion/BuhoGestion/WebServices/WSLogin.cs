﻿using BuhoGestion.Models;
using BuhoGestion.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BuhoGestion.WebServices
{
    public class WSLogin
    {
        public async Task<T> GetUsuarioLogueado<T>(string Url)
        {
            HttpClient client = new HttpClient();

            var response = await client.GetAsync(Url);
            var json = await response.Content.ReadAsStringAsync();
            var usuario = JsonConvert.DeserializeObject<T>(json);
            var user = JsonConvert.DeserializeObject<VMUserLogueado>(json);

            if (usuario != null)
            {
                Xamarin.Essentials.Preferences.Set("usuarioLogueado", user.nombreEmpleado);
                Xamarin.Essentials.Preferences.Set("idPerfilusuarioLogueado", user.idPerfil);
                //Xamarin.Essentials.Preferences.Set("idEmpleado", user.idEmpleado);
            }
            else
            {
                Xamarin.Essentials.Preferences.Set("usuarioLogueado", "");
                Xamarin.Essentials.Preferences.Set("idPerfilusuarioLogueado", 0);
            }
            return usuario;
        }
    }
}
