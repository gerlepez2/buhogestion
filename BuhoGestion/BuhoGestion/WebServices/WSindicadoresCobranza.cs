﻿using BuhoGestion.Models;
using BuhoGestion.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BuhoGestion.WebServices
{
    public class WSindicadoresCobranza
    {
        HttpClient client = new HttpClient();

        public async Task<IndicadoresCobranza> GetIndicadoresCobranza<T>(int idMes, int idEmpleado)
        {
            String url = "http://biancogestion.com/api/Indicador/calcularIndicadoresCobranza?";

            var response = await client.GetAsync(url + "idMes="+idMes + "&idEmpleado=" + idEmpleado);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var indicador = JsonConvert.DeserializeObject<IndicadoresCobranza>(content);
                return indicador;
            }
            else
            {
                return null;
            }
        }
    }
}
