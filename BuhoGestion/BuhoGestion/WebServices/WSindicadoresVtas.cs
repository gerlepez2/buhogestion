﻿using BuhoGestion.Models;
using BuhoGestion.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BuhoGestion.WebServices
{
    public class WSindicadoresVtas
    {
        HttpClient client = new HttpClient();

        public async Task<IndicadoresVta> GetIndicadoresVtas<T>(int idMes, int idDeposito)
        {
            String url = "http://biancogestion.com/api/Indicador/calcularIndicadoresVenta?";

            var response = await client.GetAsync(url + "idMes=" + idMes + "&idDeposito=" + idDeposito);

            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var listado = JsonConvert.DeserializeObject<IndicadoresVta>(content);

                    return listado;
                }
                catch(Exception ex)
                {
                    var a = ex;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
